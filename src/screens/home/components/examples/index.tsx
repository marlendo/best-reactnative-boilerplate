import React from "react";
import { View, FlatList, Image } from "react-native";
import Icon from "react-native-dynamic-vector-icons";
import RNBounceable from "@freakycoder/react-native-bounceable";
/**
 * ? Local Imports
 */
import { IHomeScreenStyle } from "screens/home/styles";
import MockData from "screens/home/mock/MockData";
import CardItem from "screens/home/components/examples/CardItem";
import Text from "@shared-components/text-wrapper/TextWrapper";
import fonts from "@fonts";
import { IPalette } from "@theme/themes";

const profileURI =
  "https://images.unsplash.com/photo-1544568100-847a948585b9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2574&q=80";

const MenuButton = ({ colors }: { colors: IPalette }) => (
  <RNBounceable>
    <Icon name="menu" type="Ionicons" color={colors.iconBlack} size={30} />
  </RNBounceable>
);

const List = ({
  handleItemPress,
  styles,
}: {
  handleItemPress: () => void;
  styles: IHomeScreenStyle;
}) => (
  <View style={styles.listContainer}>
    <FlatList
      data={MockData}
      renderItem={({ item }) => (
        <CardItem data={item} onPress={handleItemPress} />
      )}
    />
  </View>
);

const Welcome = ({ colors }: { colors: IPalette }) => {
  return (
    <>
      <Text h1 bold color={colors.text}>
        Hello Kuray
      </Text>
      <Text
        fontFamily={fonts.montserrat.lightItalic}
        color={colors.placeholder}
      >
        Welcome Back
      </Text>
    </>
  );
};

export const Header = ({
  styles,
  colors,
}: {
  styles: IHomeScreenStyle;
  colors: IPalette;
}) => (
  <View style={styles.header}>
    <MenuButton colors={colors} />
    <Image
      resizeMode="cover"
      source={{ uri: profileURI }}
      style={styles.profilePicImageStyle}
    />
  </View>
);

export const Content = ({
  styles,
  colors,
  handleItemPress,
}: {
  styles: IHomeScreenStyle;
  colors: IPalette;
  handleItemPress: () => void;
}) => {
  return (
    <View style={styles.contentContainer}>
      <Welcome colors={colors} />
      <List handleItemPress={handleItemPress} styles={styles} />
    </View>
  );
};
